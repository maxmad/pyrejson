FROM registry.devhopps.de/python/ipython

RUN pip install --user --no-cache-dir redis rejson

CMD ["ipython"]
